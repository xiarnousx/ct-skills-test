<?php

namespace App\Http\Controllers;

use App\Entities\Order;
use Illuminate\Http\Request;

use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Response;
use Validator;
class OrderController extends Controller
{
    public function getOrder()
    {
        $orders = Order::orderBy('created_at', 'desc')->get();

        $data = [
            'orders' => $orders,
        ];
        return view('welcome', $data);
    }

    public function postOrder(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'  => 'required|max:50|alpha',
            'qty'   => 'required|numeric',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
        ]);

        if ($validator->fails()) {
            $type = 'fail';
            $message = 'Invalid data input';
            return Response::json($this->formatResponse($type, $message), 200);
        }

        $order = [
            'name'  => trim($request['name']),
            'qty'   => $request['qty'],
            'price' => $request['price'],
            'total' => ($request['qty'] * $request['price']),
        ];

        $orderEntity = new Order();
        $orderEntity->json_data = json_encode($order);

        if ($orderEntity->save()) {
            return Response::json($this->formatResponse('success', 'Order saved', $order), 200);
        }

        return Response::json($this->formatResponse('fail', 'Record failed to save'), 404);

    }

    private function formatResponse($type, $message, array $data = array())
    {
        $jsonArray = [
            'type'      => $type,
            'message'   => $message,
            'data'      => [],
        ];

        if (sizeof($data) > 0 ) {
            $jsonArray['data'] = $data;
        }

        return $jsonArray;
    }
}

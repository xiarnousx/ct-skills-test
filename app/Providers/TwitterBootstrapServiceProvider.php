<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TwitterBootstrapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            base_path('vendor/twbs/bootstrap/dist') => public_path('twbs'),
        ],'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

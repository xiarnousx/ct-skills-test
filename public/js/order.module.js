/**
 * Created by xiarnousx on 6/27/16.
 */

var orderModule = (function(){

    var self = this;

    function init()
    {
        self.form = $('form#orderForm');
        self.grandTotal = 0;
        self.action = self.form.attr('action');
        self.token = $('form#orderForm > input[name=_token]').val();
        self.form.on('submit', orderModule.submitForm);

    };

    function setGrandTotal(total) {
        self.grandTotal = total;
    }

    function submitForm (event) {
        event.preventDefault();
        $.ajax({
           type : "POST",
           url : self.action,
           data : self.form.serialize(),
           success : function (result) {
               obj = result;
               if (obj.type === 'fail') {
                   $(document).trigger('data-error',[obj.message]);
               } else if (obj.type === 'success') {
                   $(document).trigger('data-success', [obj.message, obj.data]);
               }
           },
           error : function () {

           },


        });
    };

    function populateTable(event, msg, data) {
        showMsg(msg, false);
        $('table#orderContent').removeClass('orderContentDataHide');
        $('table#orderContent').addClass('orderContentDataShow');
        var html = '<tr>' +
        '<td>' + data.name + '</td>' +
        '<td>' + data.qty + '</td>' +
        '<td>' + data.price + ' $</td>' +
        '<td>' + data.total +' $</td>' +
        '</tr>';
        $('table#orderContent tr:first').after(html);
        if ($('table#orderContent tr').length > 2)
        {
            $('table#orderContent tr:last').remove();
        }
        self.grandTotal += data.total;
        var total = '<tr>' +
            '<td></td>' +
            '<td></td>' +
            '<td></td>' +
            '<td> Grand Total: ' + self.grandTotal +' $</td>' +
            '</tr>';

        $('table#orderContent tr:last').after(total);

    }

    function showError(event, msg) {
        console.log(msg);
        showMsg(msg, true);
    }

    function showMsg(msg, isError) {
        $('#status').removeClass('alert-success');
        $('#status').removeClass('alert-danger');
        $('#status').removeClass('hidden');

        if (isError) {
            $('#status').addClass('alert-danger');
        } else {
            $('#status').addClass('alert-success');
        }

        $('#status').text(msg);
    }

    return {
        init : init,
        submitForm : submitForm,
        populateTable : populateTable,
        showError : showError,
        setGrandTotal : setGrandTotal
    };
})();

(function(){
    orderModule.init();
    $(document).on('data-success', orderModule.populateTable);
    $(document).on('data-error', orderModule.showError);
})();

Author: Ihab Arnous <iarnous@gmail.com>

Description: README.md file on how about to setup environment in order to 
take skills test.

OS: Ubuntu 14.04 LTS

IDE: PhpStorm

Framework: Laravel Framework version 5.2.39

* * *

# Setting Up Environment 

1. Update System through package manager.
1. Install apache2.
1. Install mysql.
1. Install mysql db and start mysql service.
1. Secure mysql installation (answer Y when prompted).
1. Install php5 and libraries needed (php5-mcrypt needed for laravel 5).
1. Test server by creating info.php.

```bash
#!/bin/sh
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install mysql-server libapache2-mod-auth-mysql
sudo mysql_install_db
sudo service mysql start
sudo /usr/bin/mysql_secure_installation
sudo apt-get install php5 libapache2-mod-php5 php5-mysql php5-mcrypt \
php5-cli php5-curl php5-json php5-gd
touch /var/www/info.php && echo "<?php phpinfo(); " > /var/www/info.php
```

`Point your browser to http://localhost/info.php to see your php loaded
modules and php.ini and other environment variables`

# Installing composer
1. Download composer PHP dependency package manager.
1. Move composer to /user/bin.
1. Validate Composer version.

```bash
#!/bin/sh
curl -sS http://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer
composer --version 
```


# Installing Laravel latest

1. Download laravel via composer.
1. Test server running via `artisan serve`.

```bash
#!/bin/sh
composer create-project laravel/laravel /var/www/html/skilltest \
--prefer-dist
cd /var/www/html/skilltest
php artisan serve
```


# Larvel DB Migration Setup
1. Create mysql database, user and grant privileges for skilltest app.
1. Change .env key/value pairs for db values to match the created user.
```bash
#!/bin/sh
mysql -uroot -p
```bash

```mysql
#!/bin/sh
create database ct_skills_test;
create user 'ct_test_usr'@'%' identified by 'ct_test_pswd';
grant all privileges on ct_skills_test.* to ct_test_usr
flush privileges;
exit;
```

Set the database,user and password and update appropriate database driver
/config/database.php
```
DB_DATABASE=ct_skills_test
DB_USERNAME=ct_test_usr
DB_PASSWORD=ct_test_pswd
```

# Laravel 5.* artisan migration commands to set up the application
1. Install and migrate default database tables
```bash
#!/bin/sh
php artisan migrate:install
php artisan migrate
```

1. Add Captcha and Image Intervention libraries
```bash
#!/bin/sh
composer require captcha-com/laravel-captcha:"4.*"
composer require intervention/image
``` 

1. Add Service providers to /config/app.php
```php
<?php
    // ...
    "providers" => [
        // ...
        /*
         * Third Party Service Providers...
         *
         */
        LaravelCaptcha\Providers\LaravelCaptchaServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
    ],
    // ...
```

1. Publish service providers to /config
```bash
#!/bin/sh
php artisan vendor:publish
``` 

1. Installing twitter bootstrap
```bash
#!/bin/sh
composer require twbs/bootstrap
```

Create twitter service provider with public tag
```bash
#!/bin/sh
php artisan make:provider TwitterBootstrapServiceProvider 
```

Point to path inside boot method
```php
<?php
public function boot()
    {
        $this->publishes([
            base_path('vendor/twbs/bootstrap/dist') => public_path('twbs'),
        ],'public');
    }
```

Register the custom provider in config/app.php
```php
<?php
    'providers' => [
        //...
        App\Providers\TwitterBootstrapServiceProvider::class,
        //...
    ],
```

Publish the service
```bash
#!/bin/sh
php artisan vendor:publish --tag=public --force
```


<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>@yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{URL::to('twbs/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{URL::to('css/main.css')}}" type="text/css" />
    </head>
    <body>

        @yield('content')
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="{{URL::to('twbs/js/bootstrap.min.js')}}" ></script>
        @yield('scripts')
    </body>
</html>
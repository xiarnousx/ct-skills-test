@extends('layouts.master')
@section('title')
    Order Form
    @endsection

@section('content')
    <div class="container">
        <hr/>
        <div class="alert hidden" id="status"></div>
    <form id="orderForm" role="form" method="post" action="{{route('post.order')}}">
        <div class="form-group">
            <label for="name">Product Name</label>
            <input class="form-control" type="text" id="name" name="name" placeholder="Product Name" />
        </div>

        <div class="form-group">
            <label for="qty">Product Qty.</label>
            <input class="form-control" type="number" id="qty" name="qty" placeholder="Product Qty." />
        </div>

        <div class="form-group">
            <label for="price">Product Price.</label>
            <input class="form-control" type="text" id="price" name="price" placeholder="Product Price" />
        </div>
        <div class="form-group">
            {!! csrf_field() !!}
            <input type="submit" class="btn btn-primary" value="Submit Order" />
        </div>
    </form>
    <hr/>
        <?php
            $hasOrders = (count($orders) > 0);
            $class = 'orderContentData';
            $class .= $hasOrders ? 'Show' : 'Hide';
            $grandTotal = 0;
        ?>
        <table class="table {{$class}}" id="orderContent">
            <tr class="bg-primary inverse">
                <th>Product Name</th>
                <th>Qty.</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
            @foreach($orders as $order)
                <?php $orderObj = json_decode($order->json_data); ?>
                <tr>
                    <td>{{$orderObj->name}}</td>
                    <td>{{$orderObj->qty}}</td>
                    <td>{{$orderObj->price}} $</td>
                    <td>{{$orderObj->total}} $</td>
                </tr>
                <?php $grandTotal += $orderObj->total; ?>
                @endforeach

            @if($hasOrders)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Grand Total : {{$grandTotal}} $</td>
                </tr>
                @endif
        </table>
    </div>
    @endsection

@section('scripts')
    <script src="{{URL::to('js/order.module.js')}}" ></script>
    <script>
        orderModule.setGrandTotal({{$grandTotal}});
    </script>
    @endsection